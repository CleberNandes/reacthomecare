/*jshint esversion: 6 */
import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import registerServiceWorker from './registerServiceWorker';
import './index.css';
import AutorBox from './classes/Autor';
import Home from './classes/Home';
import LivroBox from './classes/Livro';
import {BrowserRouter as Router, Route, Switch
} from 'react-router-dom';
console.log('final dos imports.index.js');

ReactDOM.render((
    <Router>
        <App>
            <Switch>
                <Route exact path="/" component={Home} />
                <Route path="/autores" component={AutorBox} />
                <Route path="/livros" component={LivroBox} />
            </Switch>
        </App>
    </Router>),
    document.getElementById("root"));
registerServiceWorker();
console.log('final do index.js e do register service worker')

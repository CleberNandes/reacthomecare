/*jshint esversion: 6 */
import React, { Component } from 'react';
import PubSub from 'pubsub-js';


export default class InputGeral extends Component {

    constructor()
    {
        super();
        this.state = {msgErro:''};
    }

    render()
    {
        return (
            <div className="pure-control-group">
                <label htmlFor={this.props.id}>{this.props.label}</label> 
                <input {...this.props} />   
                <span className="error">{this.state.msgErro}</span>               
            </div>
        );
    }

    componentDidMount()
    {
        PubSub.subscribe("erro-validacao",function(i,erro){
            if(erro.field === this.props.name){
                this.setState({msgErro:erro.defaultMessage});
            }
        }.bind(this));

        PubSub.subscribe("limpa-erros",function(i,empty){

            this.setState({msgErro:''});

        }.bind(this));
    }
}
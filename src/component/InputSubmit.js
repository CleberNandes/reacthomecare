/*jshint esversion: 6 */
import React, { Component } from 'react';

export default class InputSubmit extends Component {

    render()
    {
        return (
            <div className="pure-control-group">
                <label></label> 
                <button className="pure-button pure-button-primary" 
                    type="submit">{this.props.button}</button>             
            </div>                                                    
        );
    }
}
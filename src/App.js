/*jshint esversion: 6 */
import React, { Component } from 'react';
//import logo from './logo.svg';
import './App.css';
import './css/side-menu.css';
import './css/pure-min.css';
import {Link} from 'react-router-dom';

export default class App extends Component {  

  render() {
    console.log('render');
    return (
      <div id="layout">
        <a href="/" id="menuLink" className="menu-link"></a>
        <div id="menu">
          <div className="pure-menu">                
            <a className="pure-menu-heading" href="/">Company</a>
            <ul className="pure-menu-list">
              <li className="pure-menu-item">
                <Link to="/" className="pure-menu-link">Home</Link>
              </li>
              <li className="pure-menu-item">
                <Link to="/autores" className="pure-menu-link">Autor</Link>
              </li>
              <li className="pure-menu-item">
                <Link to="/livros" className="pure-menu-link">Livro</Link>
              </li>
            </ul>
          </div>
        </div>
        <div id="main">
          {this.props.children}         
        </div>
      </div>
    );
  }
}


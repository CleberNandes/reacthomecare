/*jshint esversion: 6 */
import React, {Component} from 'react';
import $ from 'jquery';
import InputGeral from '../component/InputGeral';
import InputSubmit from '../component/InputSubmit';
import PubSub from 'pubsub-js';
import TratadorErros from './TratadorErros';

class FormularioAutor extends Component 
{
    constructor()
    {
        console.log('FormularioAutor.constructor.i');
        super();
        this.state = {nome:'',email:'',senha:''};   
        this.enviaForm = this.enviaForm.bind(this);
        console.log('FormularioAutor.constructor.final');
    }

    enviaForm(e)
    {
        e.preventDefault();
        console.log('enviando dados');
        $.ajax({
            url: 'http://localhost:8080/api/autores',
            contentType:'application/json',
            dataType:'json',
            type:'post',
            data: JSON.stringify({
                nome:this.state.nome,
                email:this.state.email,
                senha:this.state.senha
            }),
        success: function(lista){
            // disparar aviso geral de nova lista para todos os componentes envolvidos
            PubSub.publish('atualiza-autores',lista);
            this.setState({nome:'',email:'',senha:''});
        }.bind(this),
        error: function(e){
            if(e.status === 400){
                console.log('FormularioAutor.erros de validação');     
                new TratadorErros().publicaErros(e.responseJSON);
            }
        },
        beforeSend: function(){
            console.log('FormularioAutor.enviaForm.beforeSend');     
            PubSub.publish("limpa-erros",{});
        }
        });
    }

    setField(nomeInput,evento){
        this.setState({[nomeInput]:evento.target.value});
    }

    render(){
        console.log('FormularioAutor.render');
        return (
            <div className="pure-form pure-form-aligned">
                <form className="pure-form pure-form-aligned" onSubmit={this.enviaForm} 
                    method="post">
                <InputGeral id="nome" name="nome" type="text" value={this.state.nome} 
                    onChange={this.setField.bind(this, "nome")} label="Nome" />
                <InputGeral id="email" name="email" type="email" value={this.state.email} 
                    onChange={this.setField.bind(this, "email")} label="Email" />
                <InputGeral id="senha" name="senha" type="password" value={this.state.senha} 
                    onChange={this.setField.bind(this, "senha")} label="Senha" />
                <InputSubmit button="Gravar" />
                </form>
            </div>
        );
    }
}

class TabelaAutores extends Component 
{
    render()
    {
        console.log('TabelaAutores.render');
        return (
            <div>            
              <table className="pure-table">
                <thead>
                  <tr>
                    <th>Nome</th>
                    <th>email</th>
                  </tr>
                </thead>
                <tbody>
                  {
                    this.props.lista.map(function(autor){
                      return (
                        <tr key={autor.id}>
                          <td>{autor.nome}</td>
                          <td>{autor.email}</td>
                        </tr>
                      );
                    })
                  }
                </tbody>
              </table> 
            </div> 
        );
    }
}

export default class AutorBox extends Component 
{
    constructor()
    {
        super();
        this.state = {lista : []};
        console.log('AutorBox.constructor');
    }

    componentDidMount()
    {
        console.log('AutorBox.didMount.solicita.autores');
        $.ajax({
            url: 'http://localhost:8080/api/autores',
            dataType: 'json',
            success: function(data){
                console.log('AutorBox.didMount.resposta');
                this.setState({lista:data});
            }.bind(this)
        });
        PubSub.subscribe('atualiza-autores',function(topico,lista){
            console.log('AutorBox.pubsub.subscribe')
            this.setState({lista:lista});
        }.bind(this));
    }

    render()
    {
        console.log('AutorBox.render');
        return (
            <div>
                <div className="header">
                    <h1>Cadastro de Autores</h1>
                </div>
                
                <div className="content" id="content">                    
                    <FormularioAutor />
                    <TabelaAutores lista={this.state.lista}/>
                </div>
            </div>
        );
    }
}
/*jshint esversion: 6 */
import React, { Component } from 'react';
import $ from 'jquery';
import InputGeral from '../component/InputGeral';
import InputSubmit from "../component/InputSubmit";
import InputSelect from '../component/InputSelect';
import PubSub from 'pubsub-js';
import TratadorErros from './TratadorErros';

class FormularioLivro extends Component {
  
    constructor()
    {
        console.log('FormularioLivro.constructor.inicio');
        super();
        this.state = { titulo: '', preco: '', autorId:'' };
        // define o this que sera usado como this do react
        this.enviaForm = this.enviaForm.bind(this);
        console.log('FormularioLivro.constructor.final');
    }

    enviaForm(e) {
        e.preventDefault();
        console.log('FormLivro.enviando dados');
        
        var data = JSON.stringify({
          titulo: this.state.titulo,
          preco: this.state.preco,
          autorId: this.state.autorId
        });
        console.log(data);
        $.ajax({
            url: 'http://localhost:8080/api/livros',
            contentType: 'application/json',
            dataType: 'json',
            type: 'post',
            data: data,
            success: function (lista) {
                // disparar aviso geral de nova lista para todos os componentes envolvidos
                this.setState({ titulo: '', preco: '', autorId: '' });
                PubSub.publish('atualiza-livros', lista);
                console.log('FormularioLivro.erros de validação');
            }.bind(this),
            error: function (e) {
                if (e.status === 400) {
                    console.log("FormLivro.erros de validação");
                    new TratadorErros().publicaErros(e.responseJSON);
                }
            },
            beforeSend: function () {
                console.log('FormLivro.enviaForm.beforeSend');
                PubSub.publish("limpa-erros", {});
            }
        });
    }

    setField(nomeInput,evento){
        const value = evento.target.value;      
        this.setState({ [nomeInput]: value });
    }

    render() {
        console.log('FormAutor.render');
        return (
            <div className="pure-form pure-form-aligned">
                <form className="pure-form pure-form-aligned" 
                    onSubmit={this.enviaForm} method="post">
                    <InputGeral id="titulo" name="titulo" type="text" 
                        value={this.state.titulo} onChange={this.setField.bind(this,'titulo')} 
                        label='Título' />
                    <InputGeral id="preco" name="preco" type="text" 
                        value={this.state.preco} onChange={this.setField.bind(this,'preco')} 
                        label='Preço' />
                    <InputSelect id="autorId" name="autorId" label="Autor"
                        onChange={this.setField.bind(this,'autorId')} value={this.state.autorId}
                        autores={this.props.autores}/>
                    <InputSubmit button="Gravar" />
                </form>
            </div>
        );
    }
}

class TabelaLivros extends Component {
    render() {
        console.log('TabelaLivros.render');
        return (
            <div>
                <table className="pure-table">
                    <thead>
                        <tr>
                            <th>Título</th>
                            <th>Preço</th>
                            <th>Autor</th>
                        </tr>
                    </thead>
                    <tbody>
                        {this.props.lista.map(function(livro) {
                            console.log(livro)
                        return (
                            <tr key={livro.id}>
                                <td>{livro.titulo}</td>
                                <td>{livro.preco}</td>
                                <td>{livro.autor.nome}</td>
                            </tr>
                        );})}
                    </tbody>
                </table>
            </div>
        );
    }
}

export default class LivroBox extends Component {

    constructor() {
        super();
        this.state = { lista: [], autores: [] };
        console.log('LivroBox.constructor');
    }

    componentDidMount() {
        console.log('LivroBox.didMount.solicita.livros');
        $.ajax({
            url: 'http://localhost:8080/api/livros',
            dataType: 'json',
            success: function (data) {
                console.log(data)
                console.log('LivroBox.didMount.resposta');
                this.setState({ lista: data });
            }.bind(this)
        });
        PubSub.subscribe('atualiza-livros', function (topico, lista) {
            console.log('LivroBox.pubsub.subscribe')
            this.setState({ lista: lista });
        }.bind(this));

        $.ajax({
          url: "http://localhost:8080/api/autores",
          dataType: "json",
          success: function(data) {
            console.log(data);
            console.log("LivroBox.didMount.resposta");
            this.setState({ autores: data });
          }.bind(this)
        });
        PubSub.subscribe("atualiza-autores", function(topico, lista) {
            console.log("LivroBox.pubsub.subscribe");
            this.setState({ autores: lista });
          }.bind(this));
    }
    
    render() {
        console.log('LivroBox.render');
        return (
            <div>
                <div className="header">
                    <h1>Cadastro de Livros</h1>
                </div>

                <div className="content" id="content">
                    <FormularioLivro autores={this.state.autores}/>
                    <TabelaLivros lista={this.state.lista} />
                </div>
            </div>
        );
    }
}


